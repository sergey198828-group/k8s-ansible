$VMs =
@(
    [pscustomobject]@{name="k8s-lb-1";network="192.168.1.201/24";cpu=2;minRam=1GB;maxRam=2GB},
    [pscustomobject]@{name="k8s-master-1";network="192.168.1.211/24";cpu=2;minRam=2GB;maxRam=4GB},
    [pscustomobject]@{name="k8s-master-2";network="192.168.1.212/24";cpu=2;minRam=2GB;maxRam=4GB},
    [pscustomobject]@{name="k8s-master-3";network="192.168.1.213/24";cpu=2;minRam=2GB;maxRam=4GB},
    [pscustomobject]@{name="k8s-worker-1";network="192.168.1.221/24";cpu=4;minRam=2GB;maxRam=4GB},
    [pscustomobject]@{name="k8s-worker-2";network="192.168.1.222/24";cpu=4;minRam=2GB;maxRam=4GB}
)

$username = "some_user"
$password = "some_password"
$sshPublicKey = "ssh-ed25519 AAAABBBBCCCCDDDDEEEFFFFGGGGHHHHIIIIJJJJKKKKLLLMMM some_user@some_machine"

Set-Location .\hyperv-vm-provisioning
foreach ($VM in $VMs) {
    .\New-HyperVCloudImageVM.ps1 `
    -VMName $VM.name `
    -VMGeneration 2 `
    -VMProcessorCount $VM.cpu `
    -VMDynamicMemoryEnabled $true `
    -VMMemoryStartupBytes $VM.minRam `
    -VMMinimumBytes $VM.minRam `
    -VMMaximumBytes $VM.maxRam `
    -VHDSizeBytes 50GB `
    -VirtualSwitchName "External Switch" `
    -VMVersion "11.0" `
    -VMMachine_StoragePath "C:\VMs" `
    -DomainName "sergey.local" `
    -NetInterface "eth0" `
    -NetAddress $VM.network `
    -NetGateway 192.168.1.1 `
    -NameServers "192.168.1.1,8.8.8.8,4.4.4.4" `
    -GuestAdminUsername $username `
    -GuestAdminPassword $password `
    -GuestAdminSshPubKey $sshPublicKey `
    -ImageVersion "22.04"
}
