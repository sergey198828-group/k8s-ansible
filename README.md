# Kubernetes Ansible
Repository contains scripts and ansible playbooks to setup basic kubernetes clusters for education and experiments running on Ubuntu.  

## Usage
### Prerequsites
- Platform to run virtual machines
- OpenSSH client
- OpenSSL
- Python
- yq
- [Kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl)
- [HELM](https://helm.sh/docs/intro/install/)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#pip-install)
- [Kubeseal](https://github.com/bitnami-labs/sealed-secrets/releases)
### Provision virtual machines
Requirements for virtual machines:
- Master: 2CPU/4RAM
- Worker: 4CPU/4+ RAM
- 50GB Disk
- Static IPs
- Python3
- SSH server
- User in SUDO group without password prompt and SSH key imported (For ansible)
You can use any software for running Virtual Machines.  
If you are running Windows system the simplest way is to leverage Hyper-V. Install it using [following guide](https://techcommunity.microsoft.com/t5/educator-developer-blog/step-by-step-enabling-hyper-v-for-use-on-windows-11/ba-p/3745905).  
Then create External Switch. `New-VMSwitch -Name "External Switch" -NetAdapterName "Ethernet" -SwitchType External`  
Deploy virtual machines according to your needs using scripts from `vm_setup` folder:
- `New-SimpleClusterVMs.ps1` to spawn master and worker node VMs for Simple cluster.
- `New-HAClusterVMs.ps1` to spawn loadbalancer, master and worker node VMs for Highly Awailable multimaster cluster.  
**Note**: Make sure to edit scripts with your data like: IP, credentials, ssh keys etc. before running. Scripts are just examples, you are very welcome to edit them according to your needs.  
**Note**: Provision scripts based on [external repo](https://github.com/schtritoff/hyperv-vm-provisioning), please appreciate author with star at github.  
### Deploy Kubernetes cluster without master high availability (Single master)
1. Navigate to `kubernetes-simple` folder
2. Fill out variables.yaml file with your data
3. Fill out hosts inventory file with your data
4. Check connectivity with nodes from inventory:
```bash
ansible -i hosts all -m ping
```
5. [Optional] Setup local name resolution using hosts file at all nodes:
```bash
ansible-playbook -i hosts resolve.yaml
```
6. Setup local Linux user for Kubernetes at all nodes:
```bash
ansible-playbook -i hosts users.yaml
```
7. Install base Kubernetes packages and settings at all nodes:
```bash
ansible-playbook -i hosts install-k8s.yaml
```
8. Configure crictl for containerd at all nodes:
```bash
ansible-playbook -i hosts crictl.yaml
```
9. Setup master:
```bash
# Since file saved at local machine assuming user dont have NOPASSWD option for sudo configured --ask-become-pass option provided
ansible-playbook -i hosts master.yaml --ask-become-pass
```
10. Join worker nodes:
```bash
ansible-playbook -i hosts workers.yaml
```
### Deploy Kubernetes cluster with master high availability (Stacked ETCD)
1. Navigate to `kubernetes-ha-stacked-etcd` folder
2. Fill out variables.yaml file with your data
3. Fill out hosts inventory file with your data
4. Check connectivity with nodes from inventory:
```bash
ansible -i hosts all -m ping
```
5. [Optional] Setup local name resolution using hosts file at all nodes:
```bash
ansible-playbook -i hosts resolve.yaml
```
6. Setup local Linux user for Kubernetes at all nodes except loadbalancer:
```bash
ansible-playbook -i hosts users.yaml
```
7. Configure loadbalancer machine:
```bash
ansible-playbook -i hosts loadbalancer.yaml
```
8. Install base Kubernetes packages and settings at all nodes except loadbalancer:
```bash
ansible-playbook -i hosts install-k8s.yaml
```
9. Configure crictl for containerd at all nodes except loadbalancer:
```bash
ansible-playbook -i hosts crictl.yaml
```
10. Setup first master:
```bash
# Since file saved at local machine assuming user dont have NOPASSWD option for sudo configured --ask-become-pass option provided
ansible-playbook -i hosts master.yaml --ask-become-pass
```
11. Join other masters:
```bash
ansible-playbook -i hosts masters.yaml
```
12. Join worker nodes:
```bash
ansible-playbook -i hosts workers.yaml
```
### Add yourself into RBAC
Even though you can do everything using default admin.conf credential from any master node its better to issue yourself a custom certificate following [steps](https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/#normal-user) from Official Kubernetes Documentation:  
```bash
# Generate and approve certificate
openssl genrsa -out myuser.key 2048
openssl req -new -key myuser.key -out myuser.csr -subj "/CN=myuser"
cat myuser.csr | base64 | tr -d "\n"
cat <<EOF | kubectl --kubeconfig /tmp/config apply -f -
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: myuser
spec:
  request: <insert output from previous command here>
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 31536000 # 365 days
  usages:
  - client auth
EOF
kubectl --kubeconfig /tmp/config get csr
kubectl --kubeconfig /tmp/config certificate approve myuser
kubectl --kubeconfig /tmp/config get csr myuser -o jsonpath='{.status.certificate}'| base64 -d > myuser.crt
# Create rolebinding
kubectl --kubeconfig /tmp/config create clusterrolebinding cluster-admin-myuser --clusterrole=cluster-admin --user=myuser # admin, edit, view also possible roles by default
# Add to .kube/config
kubectl config set-credentials myuser --client-key=myuser.key --client-certificate=myuser.crt --embed-certs=true
sudo cat /tmp/config | yq .clusters.0.cluster.certificate-authority-data | base64 --decode > cluster-authority
kubectl config set-cluster kubernetes --server=$(sudo cat /tmp/config | yq .clusters.0.cluster.server) --certificate-authority=cluster-authority
kubectl config set-context myuser --cluster=kubernetes --user=myuser
kubectl config use-context myuser
# Remove temporary files
rm -f myuser.key myuser.csr myuser.crt
sudo rm -f /tmp/config
```
### Install essential software using ArgoCD
1. Assign labels and taints to nodes:
```bash
kubectl label node k8s-master-1 role=master
kubectl label node k8s-master-2 role=master
kubectl label node k8s-master-3 role=master
kubectl label node k8s-worker-1 role=worker
kubectl label node k8s-worker-2 role=worker
kubectl taint node k8s-worker-1 purpose=worker:NoSchedule
kubectl taint node k8s-worker-2 purpose=worker:NoSchedule
```
2. Generate certificate authority files for Cert-Manager:
```bash
# Generate certificate and encrypt it for safe storage in GIT
openssl genrsa -out ca.key 4096
openssl req -new -x509 -sha256 -days 365 -key ca.key -out ca.crt
kubectl create secret tls self-signed-ca \
--cert=ca.crt \
--key=ca.key \
--namespace cert-manager \
--dry-run=client \
--output yaml | kubeseal \
--format yaml > ./argo-cd/tls-certs/self-signed-ca-sealedsecret.yaml
# Remove temporary files
rm -f ca.crt ca.key
```
3. Deploy ArgoCD to Cluster using HELM:
```bash
helm repo add argo https://argoproj.github.io/argo-helm
helm upgrade --install argo-cd --create-namespace --namespace argo-cd argo/argo-cd --version=6.6.0 -f argo-cd/values/argo-cd.yaml
```
4. Deploy other essential application through ArgoCD (SealedSecrets controller, Metrics-Server, Ingress-NGINX controller, Cert-Manager)
```bash
kubectl apply -f argo-cd/apps/essential
```
