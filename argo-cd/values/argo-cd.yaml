## Globally shared configuration
global:
  # -- Default domain used by all components
  ## Used for ingresses, certificates, SSO, notifications, etc.
  domain: argocd.sergey.local
  # -- Default node selector for all components
  nodeSelector:
    role: worker
  # -- Default tolerations for all components
  tolerations:
  - key: purpose
    operator: Equal
    value: worker
    effect: NoSchedule
## Argo Configs
configs:
  # General Argo CD configuration
  ## Ref: https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/argocd-cm.yaml
  cm:
    accounts.svc_cicd: apiKey, login
  rbac:
    policy.csv: |
      p, role:cicd, applications, get, */*, allow
      p, role:cicd, applications, sync, */*, allow
      p, role:cicd, projects, get, *, allow
      g, svc_cicd, role:cicd
  params:
    ## Server properties
    # -- Run server without TLS
    ## NOTE: This value should be set when you generate params by other means as it changes ports used by ingress template.
    server.insecure: true
  # Argo CD sensitive data
  # Ref: https://argo-cd.readthedocs.io/en/stable/operator-manual/user-management/#sensitive-data-and-sso-client-secrets
  secret:
    # -- Bcrypt hashed admin password
    ## Argo expects the password in the secret to be bcrypt hashed. You can create this hash with
    ## `htpasswd -nbBC 10 "" $ARGO_PWD | tr -d ':\n' | sed 's/$2y/$2a/'`
    argocdServerAdminPassword: "$2a$10$J8vnhzQF61m6v3B7eeG3fO/tt.qI03qk63rivRWn0B9KkB6UTi/eq"
## Server
server:
  # TLS certificate configuration via cert-manager
  ## Ref: https://argo-cd.readthedocs.io/en/stable/operator-manual/tls/#tls-certificates-used-by-argocd-server
  certificate:
    # -- Deploy a Certificate resource (requires cert-manager)
    enabled: true
    # -- The name of the Secret that will be automatically created and managed by this Certificate resource
    secretName: argocd-server-tls
    # Certificate issuer
    ## Ref: https://cert-manager.io/docs/concepts/issuer
    issuer:
      # -- Certificate issuer kind. Either `Issuer` or `ClusterIssuer`
      kind: "ClusterIssuer"
      # -- Certificate issuer name. Eg. `letsencrypt`
      name: "self-signed"
    # Private key of the certificate
    privateKey:
      # -- Rotation policy of private key when certificate is re-issued. Either: `Never` or `Always`
      rotationPolicy: Never
      # -- The private key cryptography standards (PKCS) encoding for private key. Either: `PCKS1` or `PKCS8`
      encoding: PKCS1
      # -- Algorithm used to generate certificate private key. One of: `RSA`, `Ed25519` or `ECDSA`
      algorithm: RSA
      # -- Key bit size of the private key. If algorithm is set to `Ed25519`, size is ignored.
      size: 4096
  # Argo CD server ingress configuration
  ingress:
    # -- Enable an ingress resource for the Argo CD server
    enabled: true
    # -- Defines which ingress controller will implement the resource
    ingressClassName: "nginx"
    # -- Enable TLS configuration for the hostname defined at `server.ingress.hostname`
    ## TLS certificate will be retrieved from a TLS secret `argocd-server-tls`
    ## You can create this secret via `certificate` or `certificateSecret` option
    tls: true
